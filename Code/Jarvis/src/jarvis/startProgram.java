package jarvis;

import java.awt.Desktop; 
import java.io.*;
import java.net.*;

public class startProgram 
{
       Desktop desktop = Desktop.getDesktop();
       Process p;
       int FindObject=0;
    String getInput(String input) throws IOException
    {
        input=input.toLowerCase();
                  if(input.contains("calculator")||input.contains("calc"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("gnome-calculator");
                                    return "Calculator Opened.";
                                }catch(IOException e){}
                  }
                  else if(input.contains("vlc"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("vlc");
                                    return "VLC media player opened.";
                                }catch(IOException e){}
                  }
                  else if(input.contains("setting")||input.contains("gnome control"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("unity-control-center");
                                    return "Setting Opened.";
                                }catch(IOException e){}
                  }
                  else if(input.contains("gwenview")||input.contains("photo viewer"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("gwenview");
                                    return "Photo viewer.";
                                }catch(Exception e){}
                  }
                 else if(input.contains("file manager")||input.contains("home"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("nautilus");
                                    return "Home.";
                                }catch(Exception e){}
                  }
                 else if(input.contains("browser")||input.contains("open browser")||input.contains("firefox"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("firefox");
                                    return "Firefox opened";
                                }catch(Exception e){}
                  }
                 else if(input.contains("search google")||input.contains("site google")||input.contains("google"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Desktop desktop=Desktop.getDesktop();
                                    URI url=new URI("http://www.google.com");
                                    desktop.browse(url);
                                    return "Google Site Searched.";
                                }catch(Exception e){}
                  }
                 else if(input.contains("search facebook")||input.contains("site facebook")||input.contains("open facebook"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Desktop desktop=Desktop.getDesktop();
                                    URI url=new URI("http://www.facebook.com");
                                    desktop.browse(url);
                                    return "Facebook Site Searched.";
                                }catch(Exception e){}
                  }
                 else if(input.contains("search university")||input.contains("site uiversity")||input.contains("university site"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Desktop desktop=Desktop.getDesktop();
                                    URI url=new URI("http://www.unipune.ac.in");
                                    desktop.browse(url);
                                    return "Site Pune University Searched.";
                                }catch(Exception e){}
                  }
                 else if(input.contains("search youtube")||input.contains("site youtube")||input.contains("youtube"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Desktop desktop=Desktop.getDesktop();
                                    URI url=new URI("http://www.youtube.com");
                                    desktop.browse(url);
                                    return "Site You Tube Searched.";
                                }catch(Exception e){}
                  }
                 else if(input.contains("search gmail")||input.contains("site gmail")||input.contains("gmail"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Desktop desktop=Desktop.getDesktop();
                                    URI url=new URI("http://www.gmail.com");
                                    desktop.browse(url);
                                    return "Site Gmail Searched.";
                                }catch(Exception e){}
                  }
                 else if(input.contains("search bossmobi")||input.contains("sitebossmobi")||input.contains("site bossmobi"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Desktop desktop=Desktop.getDesktop();
                                    URI url=new URI("http://www.bossmobi.com");
                                    desktop.browse(url);
                                    return "Site Bossmobi Searched.";
                                }catch(Exception e){}
                  }
                 else if(input.contains("search hackerrank")||input.contains("hacker")||input.contains("coder"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Desktop desktop=Desktop.getDesktop();
                                    URI url=new URI("http://www.hackerrank.com");
                                    desktop.browse(url);
                                    return "Hacker rank";
                                }catch(Exception e){}
                  }

                  
                  else if(input.contains("notepad"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("notepad");
                                    return "Notepad Opened.";

                                }catch(Exception e){}
                  }
                  else if(input.contains("gedit"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("gedit");
                                    return "Gedit Opened.";

                                }catch(Exception e){}
                  }
                  else if(input.contains("vim"))
                  {
                              try
                                {
                                    FindObject=1;
                                    Process p;
                                    p=Runtime.getRuntime().exec("vim");
                                    return "Vim Opened.";

                                }catch(Exception e){}
                  }
  
                  else if(input.contains("netbeans")||input.contains("netbeans")||input.contains("net beans"))
                  {
                              try
                                {
                                    FindObject=1;
                                    p=Runtime.getRuntime().exec("netbeans");
                                    return "Net Beans Started.";
                                }catch(Exception e){}
                  }                  
                  else if(input.contains("codeblocks")||input.contains("code block")||input.contains("codeblock"))
                  {
                              try
                                {
                                    FindObject=1;
                                    p=Runtime.getRuntime().exec("codeblocks");
                                    return "Code Blocks Started.";
                                }catch(Exception e){}
                  }                  
                  else if(input.contains("evince")||input.contains("pdf reader"))
                  {
                              try
                                {
                                    FindObject=1;
                                    p=Runtime.getRuntime().exec("evince");
                                    return "P D F Reader Started.";
                                }catch(Exception e){}
                  }                  
                  else if(input.contains("start chrome")||input.contains("chrome")||input.contains("chrome browser"))
                           {
                                       try
                                {
                                    FindObject=1;
                                    p=Runtime.getRuntime().exec("chromium-browser");
                                    return "Google Chrome Started.";    
                                }catch(Exception e){}
   
                            }
                  else if(input.contains("run"))
                           {
                                       try
                                {
                                    FindObject=1;
                                    p=Runtime.getRuntime().exec("/bin/bash -c sh shell.sh");
                                    p=Runtime.getRuntime().exec("gnome-terminal ./a.out");

                                    return "Compile.";    
                                }catch(Exception e){}
   
                            }
                                  
                  else
                      return "Command Not Found (startProgram)";
    return "";
    }

    
}
